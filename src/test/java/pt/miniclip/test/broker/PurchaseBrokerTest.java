package pt.miniclip.test.broker;

import java.util.Date;

import junit.framework.Assert;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import pt.miniclip.consumers.InAppPurchaseConsumer;
import pt.miniclip.consumers.MatchBrokerConsumer;
import pt.miniclip.consumers.TestBrokerConsumer;
import pt.miniclip.publisher.BrokerPublisher;

/**
 * Unit test for simple App.
 */
public class PurchaseBrokerTest 
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
	
    public PurchaseBrokerTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( PurchaseBrokerTest.class );
    }

    /**
     * Rigourous Test :-)
     * @throws Exception 
     */
    public void testPurchaseConsumer() throws Exception
    {
    	InAppPurchaseConsumer consumer = new InAppPurchaseConsumer();
         // Start Consumer
         consumer.start();
    }
    public void testPurchase() throws Exception
    {
    	BrokerPublisher publisher = new BrokerPublisher();
    	
    	System.out.println("\nTESTE COIN PACK 1");

    	String message = "{"
 			   + "\"event_type\": \"in-app-purchase\","
 			   + "\"time\": " + Long.toString(new Date().getTime()) + ","
 			   + "\"user_id\": \"kamikat\","
 			   + "\"product_id\": \"com.miniclip.8ballpoolmult.coinpack001\","
 			   + "\"device\": \"1\","
 			   + "\"platform\": \"2\""
 			   + "}";
    	
    	
         // Send Message
         publisher.sendMessage(message, "in-app-purchase");
    }
    
}
