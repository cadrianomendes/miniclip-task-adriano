package pt.miniclip.test.broker;

import java.util.Date;

import junit.framework.Assert;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import pt.miniclip.consumers.MatchBrokerConsumer;
import pt.miniclip.consumers.TestBrokerConsumer;
import pt.miniclip.publisher.BrokerPublisher;

/**
 * Unit test for MatchBroker.
 */
public class MatchBrokerTest 
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
	
    public MatchBrokerTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( MatchBrokerTest.class );
    }

    private MatchBrokerConsumer consumer = new MatchBrokerConsumer();
    
    /**
	 * This test is starting the consumer.
	 * 
	 * 
     * @throws Exception 
     */
    public void testMatch() throws Exception   {
    	 // Start Consumer
         consumer.start();
    }    
    /**
	 * This test is sending the match infos to the broker "match".
	 * 
	 * TEST FINAL PARAMS
	 * UserName: joao1980
	 * UserLevel: 5
	 * Game Tier: 1
	 * Next Game Tier: 2
	 * CoinBalance: 100
	 * NextTierCoins: 1000
	 * Revenue: 10.0
	 * 
	 * TEST RESULT
	 * PROMO2  
	 * 
     * @throws Exception 
     */
    public void testPromo() throws Exception
    {
    	BrokerPublisher publisher = new BrokerPublisher();

    	System.out.println("\nTESTE PROMO 2");
    	
    	// Test params
    	String userAName = "mariazinhaa";
    	int userACoinBalance = 0;
    	int userALevel = 1;
    	String userBName = "joao1980";
    	int userBCoinBalance = 100;
    	int userBLevel = 5;
    	int gameTier = 1;
    	String winnerName = "joao1980";
    	
    	String message = "{" 
	    	+ "\"event_type\": \"match\"," 
	    	+ "\"time\": " + Long.toString(new Date().getTime()) + "," 
	    	+ "\"user-a\": \"" + userAName + "\"," 
			+ "\"user-b\": \"" + userBName + "\"," 
	    	+ "\"user-a-postmatch-info\":{"
				+ "\"coin-balace-after-match\":" + userACoinBalance + "," 
		    	+ "\"level-after-match\": " + userALevel + ","
				+ "\"device\": \"iPhone\"," 
		    	+ "\"plataform\": \"IOS\"" + "}," 
			+ "\"user-b-postmatch-info\":{"
				+ "\"coin-balace-after-match\":" + userBCoinBalance + "," 
				+ "\"level-after-match\": " + userBLevel + ","
				+ "\"device\": \"Galaxy\"," 
				+ "\"plataform\": \"Android\"" + "}," 
			+ "\"winner\": \"" + winnerName + "\","
			+ "\"game-tier\":" + gameTier + "," 
			+ "\"duration\":" + 99 
		+ "}";
    	
    	
         // Send Message
         publisher.sendMessage(message, "match");
         
    }
    
    /**
	 * This test is sending the match infos to the broker "match".
	 * 
	 * TEST FINAL PARAMS
	 * UserName: joao1980
	 * UserLevel: 5
	 * Game Tier: 3
	 * Next Game Tier: 4
	 * CoinBalance: 100
	 * NextTierCoins: 150000
	 * Revenue: 10.0
	 * 
	 * TEST RESULT
	 * PROMO4  
	 * 
     * @throws Exception 
     */
    public void testPromo4() throws Exception
    {
    	
    	System.out.println("\nTESTE PROMO 4");
    	
    	BrokerPublisher publisher = new BrokerPublisher();

    	// Test params
    	String userAName = "mariazinhaa";
    	int userACoinBalance = 0;
    	int userALevel = 1;
    	String userBName = "joao1980";
    	int userBCoinBalance = 100;
    	int userBLevel = 5;
    	int gameTier = 3;
    	String winnerName = "joao1980";
    	
    	String message = "{" 
	    	+ "\"event_type\": \"match\"," 
	    	+ "\"time\": " + Long.toString(new Date().getTime()) + "," 
	    	+ "\"user-a\": \"" + userAName + "\"," 
			+ "\"user-b\": \"" + userBName + "\"," 
	    	+ "\"user-a-postmatch-info\":{"
				+ "\"coin-balace-after-match\":" + userACoinBalance + "," 
		    	+ "\"level-after-match\": " + userALevel + ","
				+ "\"device\": \"iPhone\"," 
		    	+ "\"plataform\": \"IOS\"" + "}," 
			+ "\"user-b-postmatch-info\":{"
				+ "\"coin-balace-after-match\":" + userBCoinBalance + "," 
				+ "\"level-after-match\": " + userBLevel + ","
				+ "\"device\": \"Galaxy\"," 
				+ "\"plataform\": \"Android\"" + "}," 
			+ "\"winner\": \"" + winnerName + "\","
			+ "\"game-tier\":" + gameTier + "," 
			+ "\"duration\":" + 99 
		+ "}";
    	
    	
         // Send Message
         publisher.sendMessage(message, "match");
    }
    
    /**
  	 * This test is sending the match infos to the broker "match".
  	 * 
  	 * TEST FINAL PARAMS
  	 * UserName: mariazinhaa
  	 * UserLevel: 5
  	 * Game Tier: 1
  	 * Next Game Tier: 2
  	 * CoinBalance: 100
  	 * NextTierCoins: 1000
  	 * Revenue: 0.0
  	 * 
  	 * TEST RESULT
  	 * Do not sending promotion.
  	 * 
       * @throws Exception 
       */
      public void testNoPromo() throws Exception
      {
      	BrokerPublisher publisher = new BrokerPublisher();
      	
      	System.out.println("\nTESTE NO PROMO");

      	// Test params
      	String userAName = "mariazinhaa";
      	int userACoinBalance = 500;
      	int userALevel = 8;
      	String userBName = "joao1980";
      	int userBCoinBalance = 100;
      	int userBLevel = 5;
      	int gameTier = 1;
      	String winnerName = "mariazinhaa";
      	
      	String message = "{" 
  	    	+ "\"event_type\": \"match\"," 
  	    	+ "\"time\": " + Long.toString(new Date().getTime()) + "," 
  	    	+ "\"user-a\": \"" + userAName + "\"," 
  			+ "\"user-b\": \"" + userBName + "\"," 
  	    	+ "\"user-a-postmatch-info\":{"
  				+ "\"coin-balace-after-match\":" + userACoinBalance + "," 
  		    	+ "\"level-after-match\": " + userALevel + ","
  				+ "\"device\": \"iPhone\"," 
  		    	+ "\"plataform\": \"IOS\"" + "}," 
  			+ "\"user-b-postmatch-info\":{"
  				+ "\"coin-balace-after-match\":" + userBCoinBalance + "," 
  				+ "\"level-after-match\": " + userBLevel + ","
  				+ "\"device\": \"Galaxy\"," 
  				+ "\"plataform\": \"Android\"" + "}," 
  			+ "\"winner\": \"" + winnerName + "\","
  			+ "\"game-tier\":" + gameTier + "," 
  			+ "\"duration\":" + 99 
  		+ "}";
      	
      	
           // Send Message
           publisher.sendMessage(message, "match");
      }
}
