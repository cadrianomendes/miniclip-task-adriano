package pt.miniclip.test.broker;

import junit.framework.Assert;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import pt.miniclip.consumers.MatchBrokerConsumer;
import pt.miniclip.consumers.TestBrokerConsumer;
import pt.miniclip.publisher.BrokerPublisher;

/**
 * Unit test for simple App.
 */
public class BrokerTest 
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public BrokerTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( BrokerTest.class );
    }

    /**
     * Rigourous Test :-)
     * @throws Exception 
     */
    public void testBroker() throws Exception
    {
    	TestBrokerConsumer consumer = new TestBrokerConsumer();
    	BrokerPublisher publisher = new BrokerPublisher();
    	
    	String message = "{teste: test}";
    	
         // Start Consumer
         consumer.start();
         // Send Message
         publisher.sendMessage(message, "test");
    }
    
}
