package pt.miniclip.publisher;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

/**
 * This Class is used for publishing the queues.
 * 
 * 
 * @author Carlos Adriano
 *
 */

public class BrokerPublisher {

	public void sendMessage(String message, String queue) throws Exception {
		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost("localhost");
		Connection connection = factory.newConnection();
		Channel channel = connection.createChannel();

		channel.queueDeclare(queue, false, false, false, null);

		boolean exitLoop = true;
		do {
			channel.basicPublish("", queue, null, message.getBytes("UTF-8"));
			System.out.println(" [x] Sent '" + message + "' \n");
			System.out.flush();
			Thread.sleep(4000);
		} while (!exitLoop);

		channel.close();
		connection.close();
	}
}
