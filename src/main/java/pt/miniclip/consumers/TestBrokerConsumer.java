package pt.miniclip.consumers;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;
import java.util.concurrent.TimeoutException;

import org.json_voltpatches.JSONException;
import org.json_voltpatches.JSONObject;

import com.google_voltpatches.common.base.CaseFormat;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;

import pt.miniclip.db.PostgresDB;
import pt.miniclip.db.VoltDB;

/**
 * This Class access the broker "match" and send the promotion for another
 * broker.
 * 
 * @author Carlos Adriano
 *
 */

public class TestBrokerConsumer extends Thread {

	private final static String QUEUE_NAME = "test";

	public void start() {
		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost("localhost");

		Channel channel = null;

		Connection connection;
		try {
			connection = factory.newConnection();
			channel = connection.createChannel();
			channel.queueDeclare(QUEUE_NAME, false, false, false, null);
		} catch (IOException e1) {
			e1.printStackTrace();
		} catch (TimeoutException e1) {
			e1.printStackTrace();
		}

		System.out.println(" [*] Waiting for messages. To exit press CTRL+C");

		Consumer consumer = new DefaultConsumer(channel) {

			@Override
			public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties,
					byte[] body) throws IOException {
				String message = new String(body, "UTF-8");
				System.out.println(" [x] Received '" + message + "'\n");
			}

		};

		try {
			channel.basicConsume(QUEUE_NAME, true, consumer);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
