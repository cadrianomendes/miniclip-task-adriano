package pt.miniclip.consumers;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;
import java.util.concurrent.TimeoutException;

import org.json_voltpatches.JSONException;
import org.json_voltpatches.JSONObject;

import com.google_voltpatches.common.base.CaseFormat;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;

import pt.miniclip.db.PostgresDB;
import pt.miniclip.db.VoltDB;

/**
 * This Class access the broker "match" and send the promotion for another
 * broker.
 * 
 * @author Carlos Adriano
 *
 */

public class MatchBrokerConsumer extends Thread {
	
	private final static String QUEUE_NAME = "match";

	public void start() {
		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost("localhost");
		
		Channel channel = null;
		
		Connection connection;
		try {
			connection = factory.newConnection();
			channel = connection.createChannel();
			channel.queueDeclare(QUEUE_NAME, false, false, false, null);
		} catch (IOException e1) {
			e1.printStackTrace();
		} catch (TimeoutException e1) {
			e1.printStackTrace();
		}

		System.out.println(" [*] Waiting for messages. To exit press CTRL+C");

		Consumer consumer = new DefaultConsumer(channel) {

			@Override
			public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties,
					byte[] body) throws IOException {
				String message = new String(body, "UTF-8");
				System.out.println(" [x] Received '" + message + "' \n");

				JSONObject json;
				JSONObject winner = null;

				// Getting infos from json Object
				try {
					json = new JSONObject(message);
					String winnerName = json.getString("winner");

					if (winnerName.equals(json.get("user-a"))) {
						winner = new JSONObject(json.getString("user-a-postmatch-info"));
					} else if (winnerName.equals(json.get("user-b"))) {
						winner = new JSONObject(json.getString("user-b-postmatch-info"));
					} else {
						System.out.println("ERROR - Winner not found");
					}

					// PRE SETS
					int levelMin = 4;
					int revenueMin = 5;
					//

					int gameTier = json.getInt("game-tier");
					// Get how many coins the player need for the next tier.
					int nextTierCoins = getNextTierCoins(gameTier);

					int winnerCoins = winner.getInt("coin-balace-after-match");
					int winnerLevel = winner.getInt("level-after-match");
					// Get from database the player balance.
					double winnerRevenue = getUserRevenue(winnerName);

					// Check all pre conditions before send a promotion
					if (winnerLevel > levelMin) {
						if (winnerRevenue > revenueMin) {							
							if (winnerCoins <= nextTierCoins) {
								System.out.println("\n User has the pre requerements for receive a promotion. \n Sending to the broke <<PROMOTION>>...");
								// Send the promotion
								sendPromotion(getPromotion(nextTierCoins, winnerCoins));
							}
						}
					} 

					// Console final log
					System.out.println("UserName: " + winnerName);
					System.out.println("UserLevel: " + winnerLevel);
					System.out.println("Game Tier: " + gameTier);
					System.out.println("Next Game Tier: " + ++gameTier);
					System.out.println("CoinBalance: " + winnerCoins);
					System.out.println("NextTierCoins: " + nextTierCoins);
					System.out.println("Revenue: " + winnerRevenue);

				} catch (JSONException e) {
					e.printStackTrace();
				} catch (TimeoutException e) {
					e.printStackTrace();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

			}

			/**
			 * Get the promotion which the player need.
			 *
			 * @param nextTierCoins - Coins for the next tier.
			 * @param winnerCoins - User coins available after match.
			 * @return promo - The name of promotion.
			 */
			private String getPromotion(int nextTierCoins, int winnerCoins) {
				if ((nextTierCoins - winnerCoins) <= 100) {
					return "Promo1";
				} else if ((nextTierCoins - winnerCoins) <= 1000) {
					return "Promo2";
				} else if ((nextTierCoins - winnerCoins) <= 20000) {
					return "Promo3";
				} else {
					return "Promo4";
				}
			}

			/**
			 * Get the revenue of the user.
			 *
			 * @param userName - The userName.
			 * @return revenue - The user currently balance.
			 */
			private double getUserRevenue(String userName) {
				//VoltDB version
			    VoltDB voltDB = new VoltDB();
				return voltDB.getUserRevenue(userName);
				
				//~ PostgresDB postgresDB = new PostgresDB();
				//~ return postgresDB.getUserRevenue(userName);
			}

			/**
			 * Get the next game tier coins which the user need.
			 *
			 * @param gameTier - The userName.
			 * @return coins - How many coins for next tier.
			 */
			private int getNextTierCoins(int gameTier) throws JSONException {
				// NEXT TIER +1
				switch (++gameTier) {
				case 1:
					gameTier = 100;
					break;
				case 2:
					gameTier = 1000;
					break;
				case 3:
					gameTier = 10000;
					break;
				case 4:
					gameTier = 100000;
					break;
				case 5:
					gameTier = 150000;
					break;
				default:
					gameTier = 0;
					break;
				}
				return gameTier;
			}

			/**
			 * Send to another broker the promotion.
			 *
			 * @param promotion - type of promotion the player need.
			 */
			private void sendPromotion(String promotion) throws IOException, TimeoutException, InterruptedException {
				String QUEUE_NAME = "activate-promotion";

				ConnectionFactory factory = new ConnectionFactory();
				factory.setHost("localhost");
				Connection connection = factory.newConnection();
				Channel channel = connection.createChannel();

				channel.queueDeclare(QUEUE_NAME, false, false, false, null);	
				
				
				boolean exitLoop = true;
				do {
					String message = "{" + "\"event_type\": \"activate-promotion\"," + "\"time\": "
							+ Long.toString(new Date().getTime()) + "," + "\"user-id\": " + 1 + ","
							+ "\"promotion\": \"" + promotion + "\"" + "}";
					channel.basicPublish("", QUEUE_NAME, null, message.getBytes("UTF-8"));
					System.out.println(" [x] Sent '" + message + "'");
					System.out.flush();
					Thread.sleep(4000);
				} while (!exitLoop);

				channel.close();
				connection.close();
			}
		};

		try {
			channel.basicConsume(QUEUE_NAME, true, consumer);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}


}
