package pt.miniclip.consumers;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import org.json_voltpatches.JSONException;
import org.json_voltpatches.JSONObject;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;

import pt.miniclip.db.PostgresDB;
import pt.miniclip.db.VoltDB;

/**
 * This Class access the broker "in_app_purchase" and send insert the purchase register into database.
 * 
 * @author  Carlos Adriano
 *
 */

public class InAppPurchaseConsumer extends Thread {

	private final static String QUEUE_NAME = "in-app-purchase";

	public void start() {
		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost("localhost");
		
		Channel channel = null;
		
		Connection connection;
		try {
			connection = factory.newConnection();
			channel = connection.createChannel();
			channel.queueDeclare(QUEUE_NAME, false, false, false, null);
		} catch (IOException e1) {
			e1.printStackTrace();
		} catch (TimeoutException e1) {
			e1.printStackTrace();
		}

	    System.out.println(" [*] Waiting for messages. To exit press CTRL+C");

	    //VoltDB version
	    final VoltDB voltDB = new VoltDB();
	    
	    //~ final PostgresDB postgresDB = new PostgresDB();
	   
	    Consumer consumer = new DefaultConsumer(channel) {
	      @Override
	      public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body)
	          throws IOException {
	        String message = new String(body, "UTF-8");
	        System.out.println(" [x] Received '" + message + "'");
	        
	        //Getting the json object.	        
	        JSONObject json;
	        try {
				json  = new JSONObject(message);
				String eventType = json.getString("event_type");
				Long time = json.getLong("time");
				String userId = json.getString("user_id");
				String productId = json.getString("product_id");
				String device = json.getString("device");
				String platform = json.getString("platform");
				
				//VoltDB version
				voltDB.insertInAppPurchase(eventType, time, userId, productId, device, platform);
				
				//Postgres version
				//~ postgresDB.insertInAppPurchase(eventType, time, userId, productId, device, platform);				
				
				//Updating user balance
				double value = getValue(productId);
				
				//VoltDB version
				voltDB.updateUserRevenue(userId, value);
				
				//Postgres version
				//~ postgresDB.updateUserRevenue(userId, value);
				
			} catch (JSONException e) {
				e.printStackTrace();
			}
	        
	      }

	      //Method what defines which promotion value will be sent.
			private double getValue(String productId) {
				double value = 0;
				if (productId.equals("com.miniclip.8ballpoolmult.coinpack001")){
					value = 1.99;
					System.out.println("\n PACK VALUE" + value);
				} else if (productId.equals("com.miniclip.8ballpoolmult.coinpack002")){
					value = 1.99;
					System.out.println("\n PACK VALUE" + value);
				} else if (productId.equals("com.miniclip.8ballpoolmult.coinpack003")){
					value = 4.99;
					System.out.println("\n PACK VALUE" + value);
				} else if (productId.equals("com.miniclip.8ballpoolmult.coinpack004")){
					value = 9.99;
					System.out.println("\n PACK VALUE" + value);
				}
				return value;
			}
	    };
	    try {
			channel.basicConsume(QUEUE_NAME, true, consumer);
		} catch (IOException e) {
			e.printStackTrace();
		}	    
	  }

}
