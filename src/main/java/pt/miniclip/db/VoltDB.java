package pt.miniclip.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.voltdb.client.ClientStatsContext;
import org.voltdb.jdbc.IVoltDBConnection;


/** Criar 
 *  1 - Atualização de revenue (dinheiro real) do usuário
 *  2 - Lertura do revenue do usuário
 *  3 - Inserção dos dados do in app purchase (OK ou quase)
 *
 * 
 * @author  Carlos Adriano
 *
 */

public class VoltDB {

	Connection client;
	
	// Statistics manager objects from the client
    ClientStatsContext periodicStatsContext;
    ClientStatsContext fullStatsContext;

	public VoltDB() throws InterruptedException,
	ClassNotFoundException, SQLException {
		connect("localhost");
	}

	void connect(String servers) throws InterruptedException,
	ClassNotFoundException, SQLException {
		Class.forName("org.voltdb.jdbc.Driver");
		String url = "jdbc:voltdb://" + servers;

		client = DriverManager.getConnection(url, "", "");

		periodicStatsContext = ((IVoltDBConnection) client)
				.createStatsContext();
		fullStatsContext = ((IVoltDBConnection) client).createStatsContext();
	}
	
	public void insertInAppPurchase(String eventType, Long time, String userId, String productId, String device, String platform) {
		PreparedStatement iap;
		try {
			iap = client.prepareStatement("INSERT INTO in_app_purchase VALUES(?, ?, ?, ?, ?, ?)");
			iap.setString(1, eventType);
			iap.setLong(2, time);
			iap.setString(3, userId);
			iap.setString(4, productId);
			iap.setString(5, device);
			iap.setString(6, platform);
			iap.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	/**
	    * Retrive the currently user revenue. 
	    *
	    * @param  userName - The name of the user.
	    * @return userREvenue - The value available of the user.
	    */
		public double getUserRevenue(String userName) {
			double userRevenue = 0;

			try {
				String sql = "SELECT * FROM users WHERE userName = \'" + userName + "\';";

				Statement statement = client.createStatement();
				ResultSet result = statement.executeQuery(sql);

				while (result.next()) {
					userRevenue = result.getDouble(3);
					//System.out.println("User " + userName + " revenue: " + userRevenue);
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}

			return userRevenue;
		}
		/**
		    * Update the user revenue. 
		    *
		    * @param  userName - The name of the user.
		    * @param  value - Amount to be added into database records.
		    * @return userREvenue - The value available of the user.
		    */
			public int updateUserRevenue(String userName, double value) {
				int userRevenue = 0;

				try {
					String sql = "UPDATE users SET revenue = revenue + " + value + " WHERE userName = \'" + userName
							+ "\';";

					System.out.println(sql);
					Statement statement = client.createStatement();
					statement.executeUpdate(sql);

					System.out.println("User " + userName + " was added U$" + value + ",00 successfully.");

				} catch (SQLException e) {
					e.printStackTrace();
				}

				return userRevenue;
			}
	
}
