package pt.miniclip.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Time;
import java.sql.Timestamp;

import org.voltdb.client.ClientStatsContext;
import org.voltdb.jdbc.IVoltDBConnection;

/**
 * Class in charge of managing Postgres DB access
 * 
 * @method insertInAppPurchase
 * @method getUserRevenue
 * @method updateUserRevenue
 * 
 * @author Carlos Adriano
 *
 */

public class PostgresDB {

	private Connection conn;
	
	private String dbURL = "jdbc:postgresql://fmolina.com.br:5432/testdb";
	private String username = "postgres";
	private String password = "postgres";

	public PostgresDB() {
		try {
			conn = DriverManager.getConnection(dbURL, username, password);

			if (conn != null) {
				System.out.println("<<Database Connected>>");
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
	}

	/**
	 * Insert a new purchase register
	 */
	public void insertInAppPurchase(String eventType, long time, String userId, String productId, String device,
			String platform) {
		PreparedStatement iap;
		try {
			iap = conn.prepareStatement("INSERT INTO in_app_purchase VALUES(?, ?, ?, ?, ?, ?)");
			iap.setString(1, eventType);
			iap.setTimestamp(2, new Timestamp(System.currentTimeMillis()));
			iap.setString(3, userId);
			iap.setString(4, productId);
			iap.setString(5, device);
			iap.setString(6, platform);
			iap.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}
	
    /**
    * Retrive the currently user revenue. 
    *
    * @param  userName - The name of the user.
    * @return userREvenue - The value available of the user.
    */
	public double getUserRevenue(String userName) {
		double userRevenue = 0;

		try {
			String sql = "SELECT * FROM users WHERE \"userName\" = \'" + userName + "\';";

			Statement statement = conn.createStatement();
			ResultSet result = statement.executeQuery(sql);

			while (result.next()) {
				userRevenue = result.getDouble(3);
				//System.out.println("User " + userName + " revenue: " + userRevenue);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return userRevenue;
	}
	
    /**
    * Update the user revenue. 
    *
    * @param  userName - The name of the user.
    * @param  value - Amount to be added into database records.
    * @return userREvenue - The value available of the user.
    */
	public int updateUserRevenue(String userName, double value) {
		int userRevenue = 0;

		try {
			String sql = "UPDATE public.users SET revenue = revenue + " + value + " WHERE \"userName\" = \'" + userName
					+ "\';";

			System.out.println(sql);
			Statement statement = conn.createStatement();
			statement.executeUpdate(sql);

			System.out.println("User " + userName + " was added U$" + value + ",00 successfully.");

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return userRevenue;
	}

}
