##SQL##

###### in_app_purchase table 

```
#!sql

CREATE TABLE in_app_purchase (
   event_time VARCHAR(50),
   time timestamp,
   user_id VARCHAR(50),
   product_id VARCHAR(50),
   device VARCHAR(50),
   platform VARCHAR(50),
   id_purchase INTEGER
);
```


###### users table 

```
#!sql

CREATE TABLE users (
   userId integer NOT NULL,
   userName VARCHAR(50),
   revenue float
);
```


###### user data 

```
#!sql

INSERT INTO users(userId, userName, revenue) VALUES (1, 'joao1980', 10);
INSERT INTO users(userId, userName, revenue) VALUES (2, 'mariazinhaa', 0);
INSERT INTO users(userId, userName, revenue) VALUES (3, 'marcosilva', 0);
INSERT INTO users(userId, userName, revenue) VALUES (4, 'mistergood', 0);
INSERT INTO users(userId, userName, revenue) VALUES (5, 'doctorbro', 0);
INSERT INTO users(userId, userName, revenue) VALUES (6, 'darklord', 0);
INSERT INTO users(userId, userName, revenue) VALUES (7, 'kamikat', 5);
```


## Install Maven ##


```
#!linux

apt-get maven
```


## Install Git ##


```
#!linux

apt-get git
```


## Test ##


```
#!linux

cd miniclip-task-adriano
mvn install
```

*PS: The last command "mvn install" is running all Unit Tests automatically.*